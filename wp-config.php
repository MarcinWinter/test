<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'szkolenie' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'GD:>X0[x2:}R/rs$}TyvJTapkyM*ZPLEA|4d27(_R0C/WS0BKh.smj?s((XB%M0#' );
define( 'SECURE_AUTH_KEY',  '?=c.#>|O0$mmaWPch&Id~VcmH}#pBk.44_~8Yd5fsAk.d5!4EM:X:GV:@vHSaouB' );
define( 'LOGGED_IN_KEY',    's,j^>s{UM5#UZb;4WiC-wM$#BgH:{R@q+11|j0^XJ1|4A,l*`%I+uj?v]0_gG N0' );
define( 'NONCE_KEY',        'PNHwJ`]po[EDXR0:cBC?;dOs(Cpj-O,0vZ:y0~OVen{kEc7fLsZ*;Di/2r>W,f#!' );
define( 'AUTH_SALT',        'RXAyrzY)u7m|>W:F;Mc8XCdgjHp~~(:=YSRYB2GBz2XR)bE)o3RN6&5##juN*<Rj' );
define( 'SECURE_AUTH_SALT', '%S*!?cd$Zm_RcB<GKb )BKp Up@8zN]mWeDli3>_~?@gGT0X$,l2#X&TK-#_=%K!' );
define( 'LOGGED_IN_SALT',   'JDf(p&Wjg,4oIqQgLF>8 L,MET<?D5{>+U)a ]?kgDx&H^vzv2Ap2,F:]M2X=Doq' );
define( 'NONCE_SALT',       '[H(>d1%3VKyXII 4`MI5reHQ]f-()Q2%S{ecZ}w0d|wP190/}_4L+/i?|h!a2zb3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
